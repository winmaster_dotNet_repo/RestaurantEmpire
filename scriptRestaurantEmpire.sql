USE [master]
GO
/****** Object:  Database [KatarzynaBialasRestaurantEmpireLab3]    Script Date: 2016-04-27 12:09:42 ******/
CREATE DATABASE [KatarzynaBialasRestaurantEmpireLab3]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'KatarzynaBialasRestaurantEmpireLab3', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\KatarzynaBialasRestaurantEmpireLab3.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'KatarzynaBialasRestaurantEmpireLab3_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\KatarzynaBialasRestaurantEmpireLab3_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [KatarzynaBialasRestaurantEmpireLab3].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET ARITHABORT OFF 
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET  DISABLE_BROKER 
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET  MULTI_USER 
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET DB_CHAINING OFF 
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET DELAYED_DURABILITY = DISABLED 
GO
USE [KatarzynaBialasRestaurantEmpireLab3]
GO
/****** Object:  Table [dbo].[Clients]    Script Date: 2016-04-27 12:09:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clients](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Surname] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Clients] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ClientsOrders]    Script Date: 2016-04-27 12:09:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClientsOrders](
	[ID] [int] NOT NULL,
	[Date] [date] NOT NULL,
	[ClientID] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[Amount] [int] NOT NULL,
	[DishID] [int] NOT NULL,
 CONSTRAINT [PK_ClientsOrders] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Desserts]    Script Date: 2016-04-27 12:09:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Desserts](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Price] [decimal](18, 1) NOT NULL,
	[DishCategoriesID] [int] NOT NULL,
 CONSTRAINT [PK_Desserts] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DishCategories]    Script Date: 2016-04-27 12:09:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DishCategories](
	[ID] [int] NOT NULL,
	[DishCategory] [nvarchar](50) NOT NULL,
	[DishSubCategory] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_DishCategories] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Drinks]    Script Date: 2016-04-27 12:09:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Drinks](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[Price] [decimal](18, 1) NOT NULL,
	[DishCategoriesID] [int] NOT NULL,
 CONSTRAINT [PK_Drinks] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MainDishes]    Script Date: 2016-04-27 12:09:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MainDishes](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[Price] [decimal](18, 1) NOT NULL,
	[DishCategoriesID] [int] NOT NULL,
 CONSTRAINT [PK_MainDishes] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Salads]    Script Date: 2016-04-27 12:09:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Salads](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Price] [decimal](18, 1) NOT NULL,
	[DishCategoriesID] [int] NOT NULL,
 CONSTRAINT [PK_Salads] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Soups]    Script Date: 2016-04-27 12:09:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Soups](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Price] [decimal](18, 1) NOT NULL,
	[DishCategoriesID] [int] NOT NULL,
 CONSTRAINT [PK_Soups] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Starters]    Script Date: 2016-04-27 12:09:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Starters](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[Price] [decimal](18, 1) NOT NULL,
	[DishCategoriesID] [int] NOT NULL,
 CONSTRAINT [PK_Starters] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Workers]    Script Date: 2016-04-27 12:09:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Workers](
	[ID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Surname] [nvarchar](50) NOT NULL,
	[StageID] [int] NOT NULL,
	[WageID] [int] NOT NULL,
 CONSTRAINT [PK_Workers] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WorkersStages]    Script Date: 2016-04-27 12:09:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkersStages](
	[ID] [int] NOT NULL,
	[Stage] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_WorkersStages] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WorkersWages]    Script Date: 2016-04-27 12:09:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkersWages](
	[ID] [int] NOT NULL,
	[StageID] [int] NOT NULL,
	[Wage] [money] NOT NULL,
 CONSTRAINT [PK_WorkersWages] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[Clients] ([ID], [Name], [Surname]) VALUES (1, N'Tomasz', N'Kwasigroch')
INSERT [dbo].[Clients] ([ID], [Name], [Surname]) VALUES (2, N'Leszek', N'Żur')
INSERT [dbo].[Clients] ([ID], [Name], [Surname]) VALUES (3, N'Roman', N'Kiełbasa')
INSERT [dbo].[Clients] ([ID], [Name], [Surname]) VALUES (4, N'Tadeusz', N'Stęchły')
INSERT [dbo].[Clients] ([ID], [Name], [Surname]) VALUES (5, N'Marcin', N'Kusibab')
INSERT [dbo].[Clients] ([ID], [Name], [Surname]) VALUES (6, N'Elżbieta', N'Skwara')
INSERT [dbo].[Clients] ([ID], [Name], [Surname]) VALUES (7, N'Hanna', N'Mąka-Ćwikła')
INSERT [dbo].[Clients] ([ID], [Name], [Surname]) VALUES (8, N'Łukasz', N'Posępny')
INSERT [dbo].[Clients] ([ID], [Name], [Surname]) VALUES (9, N'Narcyza', N'Kolano')
INSERT [dbo].[Clients] ([ID], [Name], [Surname]) VALUES (10, N'Zenon', N'Ojdana')
INSERT [dbo].[Clients] ([ID], [Name], [Surname]) VALUES (11, N'Rafał', N'Ptaszek')
INSERT [dbo].[Clients] ([ID], [Name], [Surname]) VALUES (12, N'Zygmunt', N'Fundament')
INSERT [dbo].[Clients] ([ID], [Name], [Surname]) VALUES (13, N'Grażyna', N'Potrzeba')
INSERT [dbo].[Clients] ([ID], [Name], [Surname]) VALUES (14, N'Kacper', N'Pędziwiatr')
INSERT [dbo].[Clients] ([ID], [Name], [Surname]) VALUES (15, N'Cezary', N'Porażka')
INSERT [dbo].[ClientsOrders] ([ID], [Date], [ClientID], [ProductID], [Amount], [DishID]) VALUES (1, CAST(N'2015-02-13' AS Date), 1, 3, 2, 2)
INSERT [dbo].[ClientsOrders] ([ID], [Date], [ClientID], [ProductID], [Amount], [DishID]) VALUES (2, CAST(N'2016-04-22' AS Date), 3, 5, 8, 5)
INSERT [dbo].[ClientsOrders] ([ID], [Date], [ClientID], [ProductID], [Amount], [DishID]) VALUES (3, CAST(N'2016-03-18' AS Date), 7, 4, 2, 4)
INSERT [dbo].[Desserts] ([ID], [Name], [Price], [DishCategoriesID]) VALUES (1, N'Creme Brule', CAST(12.0 AS Decimal(18, 1)), 10)
INSERT [dbo].[Desserts] ([ID], [Name], [Price], [DishCategoriesID]) VALUES (2, N'Panna Cotta', CAST(10.0 AS Decimal(18, 1)), 10)
INSERT [dbo].[Desserts] ([ID], [Name], [Price], [DishCategoriesID]) VALUES (3, N'Suflet czekoladowy', CAST(15.0 AS Decimal(18, 1)), 10)
INSERT [dbo].[Desserts] ([ID], [Name], [Price], [DishCategoriesID]) VALUES (4, N'Pucharek lodów z owocami', CAST(12.0 AS Decimal(18, 1)), 10)
INSERT [dbo].[Desserts] ([ID], [Name], [Price], [DishCategoriesID]) VALUES (5, N'Pancakes z syropem klonowym', CAST(8.0 AS Decimal(18, 1)), 10)
INSERT [dbo].[DishCategories] ([ID], [DishCategory], [DishSubCategory]) VALUES (1, N'Przystawki', N'zimne')
INSERT [dbo].[DishCategories] ([ID], [DishCategory], [DishSubCategory]) VALUES (2, N'Przystawki', N'ciepłe')
INSERT [dbo].[DishCategories] ([ID], [DishCategory], [DishSubCategory]) VALUES (3, N'Zupy', N'')
INSERT [dbo].[DishCategories] ([ID], [DishCategory], [DishSubCategory]) VALUES (4, N'Dania główne', N'jarskie')
INSERT [dbo].[DishCategories] ([ID], [DishCategory], [DishSubCategory]) VALUES (5, N'Dania główne', N'mięsne')
INSERT [dbo].[DishCategories] ([ID], [DishCategory], [DishSubCategory]) VALUES (6, N'Sałatki', N'')
INSERT [dbo].[DishCategories] ([ID], [DishCategory], [DishSubCategory]) VALUES (7, N'Napoje', N'zimne')
INSERT [dbo].[DishCategories] ([ID], [DishCategory], [DishSubCategory]) VALUES (8, N'Napoje', N'ciepłe')
INSERT [dbo].[DishCategories] ([ID], [DishCategory], [DishSubCategory]) VALUES (9, N'Napoje', N'alkoholowe')
INSERT [dbo].[DishCategories] ([ID], [DishCategory], [DishSubCategory]) VALUES (10, N'Desery', N'')
INSERT [dbo].[Drinks] ([ID], [Name], [Type], [Price], [DishCategoriesID]) VALUES (1, N'Kawa biała', N'ciepłe', CAST(8.0 AS Decimal(18, 1)), 8)
INSERT [dbo].[Drinks] ([ID], [Name], [Type], [Price], [DishCategoriesID]) VALUES (2, N'Esspresso', N'ciepłe', CAST(5.0 AS Decimal(18, 1)), 8)
INSERT [dbo].[Drinks] ([ID], [Name], [Type], [Price], [DishCategoriesID]) VALUES (3, N'Herbata czarna', N'ciepłe', CAST(5.0 AS Decimal(18, 1)), 8)
INSERT [dbo].[Drinks] ([ID], [Name], [Type], [Price], [DishCategoriesID]) VALUES (4, N'Herbata owocowa', N'ciepłe', CAST(6.0 AS Decimal(18, 1)), 8)
INSERT [dbo].[Drinks] ([ID], [Name], [Type], [Price], [DishCategoriesID]) VALUES (5, N'Woda mineralna', N'zimne', CAST(4.0 AS Decimal(18, 1)), 7)
INSERT [dbo].[Drinks] ([ID], [Name], [Type], [Price], [DishCategoriesID]) VALUES (6, N'Sok owocowy', N'zimne', CAST(6.0 AS Decimal(18, 1)), 7)
INSERT [dbo].[Drinks] ([ID], [Name], [Type], [Price], [DishCategoriesID]) VALUES (7, N'Coco Colo', N'zimne', CAST(5.0 AS Decimal(18, 1)), 7)
INSERT [dbo].[Drinks] ([ID], [Name], [Type], [Price], [DishCategoriesID]) VALUES (8, N'Piwo', N'alkoholowe', CAST(7.0 AS Decimal(18, 1)), 9)
INSERT [dbo].[Drinks] ([ID], [Name], [Type], [Price], [DishCategoriesID]) VALUES (9, N'Wino białe', N'alkoholowe', CAST(8.0 AS Decimal(18, 1)), 9)
INSERT [dbo].[Drinks] ([ID], [Name], [Type], [Price], [DishCategoriesID]) VALUES (10, N'Wino czerwone', N'alkoholowe', CAST(8.0 AS Decimal(18, 1)), 9)
INSERT [dbo].[Drinks] ([ID], [Name], [Type], [Price], [DishCategoriesID]) VALUES (11, N'Drink', N'alkoholowe', CAST(12.0 AS Decimal(18, 1)), 9)
INSERT [dbo].[MainDishes] ([ID], [Name], [Type], [Price], [DishCategoriesID]) VALUES (1, N'Lasagne w stylu bolońskim', N'mięsne', CAST(18.5 AS Decimal(18, 1)), 5)
INSERT [dbo].[MainDishes] ([ID], [Name], [Type], [Price], [DishCategoriesID]) VALUES (2, N'Kurczak w cieście z batatami', N'mięsne', CAST(25.0 AS Decimal(18, 1)), 5)
INSERT [dbo].[MainDishes] ([ID], [Name], [Type], [Price], [DishCategoriesID]) VALUES (3, N'Vege Falafel z cukinią z grilla', N'jarskie', CAST(16.0 AS Decimal(18, 1)), 4)
INSERT [dbo].[MainDishes] ([ID], [Name], [Type], [Price], [DishCategoriesID]) VALUES (4, N'Kotlet bez kotleta podawany w kartonie', N'jarskie', CAST(25.0 AS Decimal(18, 1)), 4)
INSERT [dbo].[MainDishes] ([ID], [Name], [Type], [Price], [DishCategoriesID]) VALUES (5, N'Burger szarpany z wołowiny', N'mięsne', CAST(22.0 AS Decimal(18, 1)), 5)
INSERT [dbo].[MainDishes] ([ID], [Name], [Type], [Price], [DishCategoriesID]) VALUES (6, N'Farfale ze szpinakiem pod pierzynką', N'mięsne', CAST(23.0 AS Decimal(18, 1)), 5)
INSERT [dbo].[MainDishes] ([ID], [Name], [Type], [Price], [DishCategoriesID]) VALUES (7, N'Szparagi pod beszamelem', N'jarskie', CAST(15.0 AS Decimal(18, 1)), 4)
INSERT [dbo].[MainDishes] ([ID], [Name], [Type], [Price], [DishCategoriesID]) VALUES (8, N'Kaczka po seczuańsku na ostro', N'mięsne', CAST(35.0 AS Decimal(18, 1)), 5)
INSERT [dbo].[MainDishes] ([ID], [Name], [Type], [Price], [DishCategoriesID]) VALUES (9, N'Łosoś w warzywach', N'mięsne', CAST(25.0 AS Decimal(18, 1)), 5)
INSERT [dbo].[MainDishes] ([ID], [Name], [Type], [Price], [DishCategoriesID]) VALUES (10, N'Kurczak w sosie satay', N'mięsne', CAST(19.0 AS Decimal(18, 1)), 5)
INSERT [dbo].[Salads] ([ID], [Name], [Price], [DishCategoriesID]) VALUES (1, N'Cezar', CAST(18.0 AS Decimal(18, 1)), 6)
INSERT [dbo].[Salads] ([ID], [Name], [Price], [DishCategoriesID]) VALUES (2, N'Sałatka szefa kuchni', CAST(16.5 AS Decimal(18, 1)), 6)
INSERT [dbo].[Salads] ([ID], [Name], [Price], [DishCategoriesID]) VALUES (3, N'Latający Holender', CAST(15.9 AS Decimal(18, 1)), 6)
INSERT [dbo].[Salads] ([ID], [Name], [Price], [DishCategoriesID]) VALUES (4, N'Burakowo-granatowe carpaccio', CAST(12.0 AS Decimal(18, 1)), 6)
INSERT [dbo].[Salads] ([ID], [Name], [Price], [DishCategoriesID]) VALUES (5, N'Grilowany kurczak na zielsku', CAST(17.0 AS Decimal(18, 1)), 6)
INSERT [dbo].[Soups] ([ID], [Name], [Price], [DishCategoriesID]) VALUES (1, N'Zupa rybna kaprów', CAST(16.0 AS Decimal(18, 1)), 3)
INSERT [dbo].[Soups] ([ID], [Name], [Price], [DishCategoriesID]) VALUES (2, N'Rosół', CAST(12.0 AS Decimal(18, 1)), 3)
INSERT [dbo].[Soups] ([ID], [Name], [Price], [DishCategoriesID]) VALUES (3, N'Krem z pomidorów i papryki', CAST(15.0 AS Decimal(18, 1)), 3)
INSERT [dbo].[Soups] ([ID], [Name], [Price], [DishCategoriesID]) VALUES (4, N'Krem z dynii z bekonem', CAST(13.0 AS Decimal(18, 1)), 3)
INSERT [dbo].[Soups] ([ID], [Name], [Price], [DishCategoriesID]) VALUES (5, N'Beef Strogonov', CAST(17.0 AS Decimal(18, 1)), 3)
INSERT [dbo].[Starters] ([ID], [Name], [Type], [Price], [DishCategoriesID]) VALUES (1, N'Tatar z łososia', N'zimne', CAST(15.0 AS Decimal(18, 1)), 1)
INSERT [dbo].[Starters] ([ID], [Name], [Type], [Price], [DishCategoriesID]) VALUES (2, N'Fois Gras', N'zimne', CAST(25.0 AS Decimal(18, 1)), 1)
INSERT [dbo].[Starters] ([ID], [Name], [Type], [Price], [DishCategoriesID]) VALUES (3, N'Suflet serowy', N'ciepłe', CAST(20.0 AS Decimal(18, 1)), 2)
INSERT [dbo].[Starters] ([ID], [Name], [Type], [Price], [DishCategoriesID]) VALUES (4, N'Krewetki grilowane', N'ciepłe', CAST(22.0 AS Decimal(18, 1)), 2)
INSERT [dbo].[Starters] ([ID], [Name], [Type], [Price], [DishCategoriesID]) VALUES (5, N'Krążki cebulowe', N'ciepłe', CAST(12.0 AS Decimal(18, 1)), 2)
INSERT [dbo].[Starters] ([ID], [Name], [Type], [Price], [DishCategoriesID]) VALUES (6, N'Tatar cielęcy z jajkiem', N'zimne', CAST(18.0 AS Decimal(18, 1)), 1)
INSERT [dbo].[Workers] ([ID], [Name], [Surname], [StageID], [WageID]) VALUES (1, N'Janusz', N'Mieszadło', 1, 1)
INSERT [dbo].[Workers] ([ID], [Name], [Surname], [StageID], [WageID]) VALUES (2, N'Andrzej', N'Talarek', 1, 1)
INSERT [dbo].[Workers] ([ID], [Name], [Surname], [StageID], [WageID]) VALUES (3, N'Rafał', N'Stolnica', 1, 1)
INSERT [dbo].[Workers] ([ID], [Name], [Surname], [StageID], [WageID]) VALUES (4, N'Jan', N'Masło', 2, 2)
INSERT [dbo].[Workers] ([ID], [Name], [Surname], [StageID], [WageID]) VALUES (5, N'Krystian', N'Kaufer', 3, 3)
INSERT [dbo].[Workers] ([ID], [Name], [Surname], [StageID], [WageID]) VALUES (6, N'Gustav', N'Gustoue-Moran', 4, 4)
INSERT [dbo].[Workers] ([ID], [Name], [Surname], [StageID], [WageID]) VALUES (7, N'Jaques', N'Petit-Lebon', 5, 5)
INSERT [dbo].[Workers] ([ID], [Name], [Surname], [StageID], [WageID]) VALUES (8, N'Ludwik', N'Gąbeczka', 6, 6)
INSERT [dbo].[Workers] ([ID], [Name], [Surname], [StageID], [WageID]) VALUES (9, N'Jakub', N'Przystawa', 7, 7)
INSERT [dbo].[Workers] ([ID], [Name], [Surname], [StageID], [WageID]) VALUES (10, N'Ewelina', N'Marchewka', 7, 7)
INSERT [dbo].[Workers] ([ID], [Name], [Surname], [StageID], [WageID]) VALUES (11, N'Ryszard', N'Golonka', 7, 7)
INSERT [dbo].[Workers] ([ID], [Name], [Surname], [StageID], [WageID]) VALUES (12, N'Maksymilian', N'Daniels', 8, 8)
INSERT [dbo].[Workers] ([ID], [Name], [Surname], [StageID], [WageID]) VALUES (13, N'Milena', N'Amarena', 8, 8)
INSERT [dbo].[Workers] ([ID], [Name], [Surname], [StageID], [WageID]) VALUES (14, N'Zenon', N'Latawiec', 9, 9)
INSERT [dbo].[Workers] ([ID], [Name], [Surname], [StageID], [WageID]) VALUES (15, N'Jerzy', N'Bosman', 10, 10)
INSERT [dbo].[WorkersStages] ([ID], [Stage]) VALUES (1, N'Kucharz')
INSERT [dbo].[WorkersStages] ([ID], [Stage]) VALUES (2, N'Pomocnik Kuchenny')
INSERT [dbo].[WorkersStages] ([ID], [Stage]) VALUES (3, N'Zaopatrzenie')
INSERT [dbo].[WorkersStages] ([ID], [Stage]) VALUES (4, N'Szef Kuchni')
INSERT [dbo].[WorkersStages] ([ID], [Stage]) VALUES (5, N'Zastępca szefa kuchni')
INSERT [dbo].[WorkersStages] ([ID], [Stage]) VALUES (6, N'Zmywak')
INSERT [dbo].[WorkersStages] ([ID], [Stage]) VALUES (7, N'Kelner')
INSERT [dbo].[WorkersStages] ([ID], [Stage]) VALUES (8, N'Barman')
INSERT [dbo].[WorkersStages] ([ID], [Stage]) VALUES (9, N'Konserwator powierzchni płaskich')
INSERT [dbo].[WorkersStages] ([ID], [Stage]) VALUES (10, N'Manager')
INSERT [dbo].[WorkersWages] ([ID], [StageID], [Wage]) VALUES (1, 1, 2250.0000)
INSERT [dbo].[WorkersWages] ([ID], [StageID], [Wage]) VALUES (2, 2, 1200.0000)
INSERT [dbo].[WorkersWages] ([ID], [StageID], [Wage]) VALUES (3, 3, 2300.0000)
INSERT [dbo].[WorkersWages] ([ID], [StageID], [Wage]) VALUES (4, 4, 3250.0000)
INSERT [dbo].[WorkersWages] ([ID], [StageID], [Wage]) VALUES (5, 5, 2800.0000)
INSERT [dbo].[WorkersWages] ([ID], [StageID], [Wage]) VALUES (6, 6, 850.0000)
INSERT [dbo].[WorkersWages] ([ID], [StageID], [Wage]) VALUES (7, 7, 1800.0000)
INSERT [dbo].[WorkersWages] ([ID], [StageID], [Wage]) VALUES (8, 8, 1500.0000)
INSERT [dbo].[WorkersWages] ([ID], [StageID], [Wage]) VALUES (9, 9, 1000.0000)
INSERT [dbo].[WorkersWages] ([ID], [StageID], [Wage]) VALUES (10, 10, 5000.0000)
ALTER TABLE [dbo].[ClientsOrders]  WITH CHECK ADD  CONSTRAINT [FK_ClientsOrders_Clients] FOREIGN KEY([ClientID])
REFERENCES [dbo].[Clients] ([ID])
GO
ALTER TABLE [dbo].[ClientsOrders] CHECK CONSTRAINT [FK_ClientsOrders_Clients]
GO
ALTER TABLE [dbo].[ClientsOrders]  WITH CHECK ADD  CONSTRAINT [FK_ClientsOrders_DishCategories] FOREIGN KEY([ProductID])
REFERENCES [dbo].[DishCategories] ([ID])
GO
ALTER TABLE [dbo].[ClientsOrders] CHECK CONSTRAINT [FK_ClientsOrders_DishCategories]
GO
ALTER TABLE [dbo].[Desserts]  WITH CHECK ADD  CONSTRAINT [FK_Desserts_DishCategories] FOREIGN KEY([DishCategoriesID])
REFERENCES [dbo].[DishCategories] ([ID])
GO
ALTER TABLE [dbo].[Desserts] CHECK CONSTRAINT [FK_Desserts_DishCategories]
GO
ALTER TABLE [dbo].[Drinks]  WITH CHECK ADD  CONSTRAINT [FK_Drinks_DishCategories] FOREIGN KEY([DishCategoriesID])
REFERENCES [dbo].[DishCategories] ([ID])
GO
ALTER TABLE [dbo].[Drinks] CHECK CONSTRAINT [FK_Drinks_DishCategories]
GO
ALTER TABLE [dbo].[MainDishes]  WITH CHECK ADD  CONSTRAINT [FK_MainDishes_DishCategories] FOREIGN KEY([DishCategoriesID])
REFERENCES [dbo].[DishCategories] ([ID])
GO
ALTER TABLE [dbo].[MainDishes] CHECK CONSTRAINT [FK_MainDishes_DishCategories]
GO
ALTER TABLE [dbo].[Salads]  WITH CHECK ADD  CONSTRAINT [FK_Salads_DishCategories] FOREIGN KEY([DishCategoriesID])
REFERENCES [dbo].[DishCategories] ([ID])
GO
ALTER TABLE [dbo].[Salads] CHECK CONSTRAINT [FK_Salads_DishCategories]
GO
ALTER TABLE [dbo].[Soups]  WITH CHECK ADD  CONSTRAINT [FK_Soups_DishCategories] FOREIGN KEY([DishCategoriesID])
REFERENCES [dbo].[DishCategories] ([ID])
GO
ALTER TABLE [dbo].[Soups] CHECK CONSTRAINT [FK_Soups_DishCategories]
GO
ALTER TABLE [dbo].[Starters]  WITH CHECK ADD  CONSTRAINT [FK_Starters_DishCategories] FOREIGN KEY([DishCategoriesID])
REFERENCES [dbo].[DishCategories] ([ID])
GO
ALTER TABLE [dbo].[Starters] CHECK CONSTRAINT [FK_Starters_DishCategories]
GO
ALTER TABLE [dbo].[Workers]  WITH CHECK ADD  CONSTRAINT [FK_Workers_WorkersStages] FOREIGN KEY([StageID])
REFERENCES [dbo].[WorkersStages] ([ID])
GO
ALTER TABLE [dbo].[Workers] CHECK CONSTRAINT [FK_Workers_WorkersStages]
GO
ALTER TABLE [dbo].[Workers]  WITH CHECK ADD  CONSTRAINT [FK_Workers_WorkersWages] FOREIGN KEY([WageID])
REFERENCES [dbo].[WorkersWages] ([ID])
GO
ALTER TABLE [dbo].[Workers] CHECK CONSTRAINT [FK_Workers_WorkersWages]
GO
ALTER TABLE [dbo].[WorkersWages]  WITH CHECK ADD  CONSTRAINT [FK_WorkersWages_WorkersStages] FOREIGN KEY([StageID])
REFERENCES [dbo].[WorkersStages] ([ID])
GO
ALTER TABLE [dbo].[WorkersWages] CHECK CONSTRAINT [FK_WorkersWages_WorkersStages]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Starters', @level2type=N'COLUMN',@level2name=N'Price'
GO
USE [master]
GO
ALTER DATABASE [KatarzynaBialasRestaurantEmpireLab3] SET  READ_WRITE 
GO
