﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KatarzynaBialasLab3RestaurantEmpire
{
    public class MenuDataGridview
    {
        public static void GetAllMenuPieces(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select DishCategory as Kategoria, DishSubCategory as Rodzaj from DishCategories", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }

        public static void GetMenuCategory(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView, string category)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select Name as Nazwa, Price as Cena from " + category, sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }

        public static void GetMenuCategoryWhere(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView, string category, string where)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select Name as Nazwa, Price as Cena from " + category + " where Type='"+where+"'", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }

        public static void GetAllWorkers(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select a.Name as Imię, a.Surname as Nazwisko, b.Stage as Stanowisko, c.Wage as Pensja from Workers a join WorkersStages b on a.StageID = b.ID join WorkersWages c on a.WageID = c.ID",sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }

        public static void GetAllWorkersWithStage(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView, string stage)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select a.Name as Imię, a.Surname as Nazwisko, b.Stage as Stanowisko, c.Wage as Pensja from Workers a join WorkersStages b on a.StageID = b.ID join WorkersWages c on a.WageID = c.ID WHERE Stage='"+stage+"'", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }

        public static void GetAllWorkersWithWage(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView, double wageMin, double wageMax)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select a.Name as Imię, a.Surname as Nazwisko, b.Stage as Stanowisko, c.Wage as Pensja from Workers a join WorkersStages b on a.StageID = b.ID join WorkersWages c on a.WageID = c.ID WHERE Wage >="+ wageMin.ToString() + " and Wage <= " +wageMax.ToString(), sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }

        public static void GetAllClients(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select ID as lp, Name as Imie, Surname as Nazwisko from Clients", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }

        public static void GetAllClientOrders(SqlConnection sqlConnection, SqlDataAdapter sqlDataAdapter, DataGridView dataGridView)
        {
            dataGridView.DataSource = null;
            sqlDataAdapter = new SqlDataAdapter("Select a.ID as lp, a.Date as Data, b.Surname as Nazwisko, c.DishCategory as Produkt,  a.Amount as Ilość  from ClientsOrders a  join Clients b on a.ClientID = b.ID  join DishCategories c on a.ProductID = c.ID ", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            dataGridView.DataSource = dataTable;
        }

        public static string GetMenuCategoryNameFromDatabase(SqlConnection sqlConnection, string dataMenu)
        {

            SqlCommand sqlCommand = new SqlCommand("Select TOP 1 [MenuCategory] from DishCategories Where DishCategory='"+dataMenu+"'", sqlConnection);
            sqlConnection.Open();
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            sqlDataReader.Read();
            string menuCategoryName=sqlDataReader.GetString(0);
            sqlConnection.Close();
            return menuCategoryName;
        }

        public static Image ByteArrayToImage(byte[] bytesImage)
        {
            Image image = null;
            using (MemoryStream memoryStream = new MemoryStream(bytesImage))
            {
                image = Image.FromStream(memoryStream);
            }
            return image;
        }

        public static void GetDishImage(SqlConnection sqlConnection, string dataMenu, string dataCategory, PictureBox pictureBox)
        {
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter("SELECT DishImage FROM " + dataMenu + " WHERE Name='" + dataCategory + "'", sqlConnection);
            DataTable dataTable = new DataTable();
            sqlDataAdapter.Fill(dataTable);
            byte[] imgBytes=null;

            foreach (DataRow dataRow in dataTable.Rows)
            {
               imgBytes = (byte[])dataRow["DishImage"];
                
                TypeConverter typeConverter = TypeDescriptor.GetConverter(typeof(Bitmap));
               /* bitmap = (Bitmap)typeConverter.ConvertFrom(imgBytes);*/
            }

            /*
            SqlCommand sqlCommand = new SqlCommand("SELECT DishImage FROM " + dataMenu + " WHERE Name='" + dataCategory + "'", sqlConnection);
            sqlConnection.Open();
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
            sqlDataReader.Read();
            object[] dataAdress=null;
            sqlDataReader.GetValues(dataAdress);
            sqlConnection.Close();

            byte[] imageBinarry = new byte[dataAdress.Length];

            for(int i=0;  i<dataAdress.Length; i++)
            {
                imageBinarry[i] = Byte.Parse(dataAdress[i].ToString());
            }
          */
            Image dishImage = ByteArrayToImage(imgBytes);
            pictureBox.Image =dishImage ;
        }
    }
}
