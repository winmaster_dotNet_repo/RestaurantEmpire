﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KatarzynaBialasLab3RestaurantEmpire
{
    public partial class FormWorkers : Form
    {
        private SqlConnection sqlConnection;
        private SqlDataAdapter sqlDataAdapter;

        public FormWorkers()
        {
            InitializeComponent();
            sqlConnection = new SqlConnection("Data Source = localhost; database = KatarzynaBialasRestaurantEmpireLab3; Trusted_Connection= yes");

        }

        private void buttonShowWorkers_Click(object sender, EventArgs e)
        {
            MenuDataGridview.GetAllWorkers(sqlConnection, sqlDataAdapter, dataGridViewWorkers);
        }

        private void buttonShowWorkersWithStage_Click(object sender, EventArgs e)
        {
            String stage = textBoxStage.Text;
            MenuDataGridview.GetAllWorkersWithStage(sqlConnection, sqlDataAdapter, dataGridViewWorkers, stage);
        }

        private void buttonShowWorkersWithWage_Click(object sender, EventArgs e)
        {
            if(textBoxWageBig.Text!="" && textBoxWageSmall.Text!="")
            {
                double wageMin = Double.Parse(textBoxWageSmall.Text);
                double wageMax = Double.Parse(textBoxWageBig.Text);
                MenuDataGridview.GetAllWorkersWithWage(sqlConnection, sqlDataAdapter, dataGridViewWorkers, wageMax, wageMin);
            }
            else
            {
                MessageBox.Show("Podano błędne wartości!");
            }
        }

        private void buttonDropWorker_Click(object sender, EventArgs e)
        {

        }

        private void buttonEmployWorker_Click(object sender, EventArgs e)
        {

        }
    }
}
