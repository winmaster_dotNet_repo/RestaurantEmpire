﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KatarzynaBialasLab3RestaurantEmpire
{
    public partial class FormClients : Form
    {
        private SqlConnection sqlConnection;
        private SqlDataAdapter sqlDataAdapter;

        public FormClients()
        {
            InitializeComponent();
            sqlConnection = new SqlConnection("Data Source = localhost; database = KatarzynaBialasRestaurantEmpireLab3; Trusted_Connection= yes");
        }

        private void buttonShowClients_Click(object sender, EventArgs e)
        {
            MenuDataGridview.GetAllClients(sqlConnection, sqlDataAdapter, dataGridViewClients);
        }

        private void buttonShowClientsOrders_Click(object sender, EventArgs e)
        {
            MenuDataGridview.GetAllClientOrders(sqlConnection, sqlDataAdapter, dataGridViewClientsOrdersList);
        }
    }
}
