﻿namespace KatarzynaBialasLab3RestaurantEmpire
{
    partial class FormWorkers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridViewWorkers = new System.Windows.Forms.DataGridView();
            this.labelListOfWorkers = new System.Windows.Forms.Label();
            this.buttonShowWorkers = new System.Windows.Forms.Button();
            this.buttonDropWorker = new System.Windows.Forms.Button();
            this.buttonEmployWorker = new System.Windows.Forms.Button();
            this.textBoxStage = new System.Windows.Forms.TextBox();
            this.labelStage = new System.Windows.Forms.Label();
            this.buttonShowWorkersWithStage = new System.Windows.Forms.Button();
            this.labelWageBigArgument = new System.Windows.Forms.Label();
            this.textBoxWageBig = new System.Windows.Forms.TextBox();
            this.labelWageSmallArgument = new System.Windows.Forms.Label();
            this.textBoxWageSmall = new System.Windows.Forms.TextBox();
            this.buttonShowWorkersWithWage = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewWorkers)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewWorkers
            // 
            this.dataGridViewWorkers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewWorkers.Location = new System.Drawing.Point(12, 58);
            this.dataGridViewWorkers.Name = "dataGridViewWorkers";
            this.dataGridViewWorkers.Size = new System.Drawing.Size(477, 223);
            this.dataGridViewWorkers.TabIndex = 0;
            // 
            // labelListOfWorkers
            // 
            this.labelListOfWorkers.AutoSize = true;
            this.labelListOfWorkers.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F);
            this.labelListOfWorkers.Location = new System.Drawing.Point(156, 24);
            this.labelListOfWorkers.Name = "labelListOfWorkers";
            this.labelListOfWorkers.Size = new System.Drawing.Size(167, 22);
            this.labelListOfWorkers.TabIndex = 1;
            this.labelListOfWorkers.Text = "Lista Zatrudnionych";
            // 
            // buttonShowWorkers
            // 
            this.buttonShowWorkers.Location = new System.Drawing.Point(12, 301);
            this.buttonShowWorkers.Name = "buttonShowWorkers";
            this.buttonShowWorkers.Size = new System.Drawing.Size(193, 23);
            this.buttonShowWorkers.TabIndex = 2;
            this.buttonShowWorkers.Text = "Pokaż wszystkich pracowników";
            this.buttonShowWorkers.UseVisualStyleBackColor = true;
            this.buttonShowWorkers.Click += new System.EventHandler(this.buttonShowWorkers_Click);
            // 
            // buttonDropWorker
            // 
            this.buttonDropWorker.Enabled = false;
            this.buttonDropWorker.Location = new System.Drawing.Point(12, 330);
            this.buttonDropWorker.Name = "buttonDropWorker";
            this.buttonDropWorker.Size = new System.Drawing.Size(193, 23);
            this.buttonDropWorker.TabIndex = 3;
            this.buttonDropWorker.Text = "Zwolnij pracownika";
            this.buttonDropWorker.UseVisualStyleBackColor = true;
            this.buttonDropWorker.Click += new System.EventHandler(this.buttonDropWorker_Click);
            // 
            // buttonEmployWorker
            // 
            this.buttonEmployWorker.Enabled = false;
            this.buttonEmployWorker.Location = new System.Drawing.Point(12, 359);
            this.buttonEmployWorker.Name = "buttonEmployWorker";
            this.buttonEmployWorker.Size = new System.Drawing.Size(193, 23);
            this.buttonEmployWorker.TabIndex = 4;
            this.buttonEmployWorker.Text = "Zatrudnij Pracownika";
            this.buttonEmployWorker.UseVisualStyleBackColor = true;
            this.buttonEmployWorker.Click += new System.EventHandler(this.buttonEmployWorker_Click);
            // 
            // textBoxStage
            // 
            this.textBoxStage.Location = new System.Drawing.Point(528, 83);
            this.textBoxStage.Name = "textBoxStage";
            this.textBoxStage.Size = new System.Drawing.Size(179, 20);
            this.textBoxStage.TabIndex = 5;
            // 
            // labelStage
            // 
            this.labelStage.AutoSize = true;
            this.labelStage.Location = new System.Drawing.Point(525, 67);
            this.labelStage.Name = "labelStage";
            this.labelStage.Size = new System.Drawing.Size(182, 13);
            this.labelStage.TabIndex = 6;
            this.labelStage.Text = "Pokaż wg stanowiska (podaj nazwe):";
            // 
            // buttonShowWorkersWithStage
            // 
            this.buttonShowWorkersWithStage.Location = new System.Drawing.Point(528, 109);
            this.buttonShowWorkersWithStage.Name = "buttonShowWorkersWithStage";
            this.buttonShowWorkersWithStage.Size = new System.Drawing.Size(75, 23);
            this.buttonShowWorkersWithStage.TabIndex = 7;
            this.buttonShowWorkersWithStage.Text = "pokaż";
            this.buttonShowWorkersWithStage.UseVisualStyleBackColor = true;
            this.buttonShowWorkersWithStage.Click += new System.EventHandler(this.buttonShowWorkersWithStage_Click);
            // 
            // labelWageBigArgument
            // 
            this.labelWageBigArgument.AutoSize = true;
            this.labelWageBigArgument.Location = new System.Drawing.Point(525, 155);
            this.labelWageBigArgument.Name = "labelWageBigArgument";
            this.labelWageBigArgument.Size = new System.Drawing.Size(121, 13);
            this.labelWageBigArgument.TabIndex = 8;
            this.labelWageBigArgument.Text = "Pokaż wg pensji >= niż :";
            // 
            // textBoxWageBig
            // 
            this.textBoxWageBig.Location = new System.Drawing.Point(528, 171);
            this.textBoxWageBig.Name = "textBoxWageBig";
            this.textBoxWageBig.Size = new System.Drawing.Size(179, 20);
            this.textBoxWageBig.TabIndex = 9;
            // 
            // labelWageSmallArgument
            // 
            this.labelWageSmallArgument.AutoSize = true;
            this.labelWageSmallArgument.Location = new System.Drawing.Point(525, 207);
            this.labelWageSmallArgument.Name = "labelWageSmallArgument";
            this.labelWageSmallArgument.Size = new System.Drawing.Size(121, 13);
            this.labelWageSmallArgument.TabIndex = 10;
            this.labelWageSmallArgument.Text = "Pokaż wg pensji <= niż :";
            // 
            // textBoxWageSmall
            // 
            this.textBoxWageSmall.Location = new System.Drawing.Point(528, 223);
            this.textBoxWageSmall.Name = "textBoxWageSmall";
            this.textBoxWageSmall.Size = new System.Drawing.Size(179, 20);
            this.textBoxWageSmall.TabIndex = 11;
            // 
            // buttonShowWorkersWithWage
            // 
            this.buttonShowWorkersWithWage.Location = new System.Drawing.Point(528, 249);
            this.buttonShowWorkersWithWage.Name = "buttonShowWorkersWithWage";
            this.buttonShowWorkersWithWage.Size = new System.Drawing.Size(75, 23);
            this.buttonShowWorkersWithWage.TabIndex = 12;
            this.buttonShowWorkersWithWage.Text = "pokaż";
            this.buttonShowWorkersWithWage.UseVisualStyleBackColor = true;
            this.buttonShowWorkersWithWage.Click += new System.EventHandler(this.buttonShowWorkersWithWage_Click);
            // 
            // FormWorkers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(766, 393);
            this.Controls.Add(this.buttonShowWorkersWithWage);
            this.Controls.Add(this.textBoxWageSmall);
            this.Controls.Add(this.labelWageSmallArgument);
            this.Controls.Add(this.textBoxWageBig);
            this.Controls.Add(this.labelWageBigArgument);
            this.Controls.Add(this.buttonShowWorkersWithStage);
            this.Controls.Add(this.labelStage);
            this.Controls.Add(this.textBoxStage);
            this.Controls.Add(this.buttonEmployWorker);
            this.Controls.Add(this.buttonDropWorker);
            this.Controls.Add(this.buttonShowWorkers);
            this.Controls.Add(this.labelListOfWorkers);
            this.Controls.Add(this.dataGridViewWorkers);
            this.Name = "FormWorkers";
            this.Text = "Pracownicy";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewWorkers)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewWorkers;
        private System.Windows.Forms.Label labelListOfWorkers;
        private System.Windows.Forms.Button buttonShowWorkers;
        private System.Windows.Forms.Button buttonDropWorker;
        private System.Windows.Forms.Button buttonEmployWorker;
        private System.Windows.Forms.TextBox textBoxStage;
        private System.Windows.Forms.Label labelStage;
        private System.Windows.Forms.Button buttonShowWorkersWithStage;
        private System.Windows.Forms.Label labelWageBigArgument;
        private System.Windows.Forms.TextBox textBoxWageBig;
        private System.Windows.Forms.Label labelWageSmallArgument;
        private System.Windows.Forms.TextBox textBoxWageSmall;
        private System.Windows.Forms.Button buttonShowWorkersWithWage;
    }
}