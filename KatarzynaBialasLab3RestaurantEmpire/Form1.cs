﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KatarzynaBialasLab3RestaurantEmpire
{
    public partial class FormRestaurantEmpire : Form
    {
        private SqlConnection sqlConnection;
        private SqlDataAdapter sqlDataAdapter;

        public FormRestaurantEmpire()
        {
            InitializeComponent();
            sqlConnection = new SqlConnection("Data Source = localhost; database = KatarzynaBialasRestaurantEmpireLab3; Trusted_Connection= yes");
        }

        private void buttonShowMenu_Click(object sender, EventArgs e)
        {
            MenuDataGridview.GetAllMenuPieces(sqlConnection, sqlDataAdapter, dataGridViewMenu);
            buttonGetCategory.Enabled = true;
        }

        private void buttonGetCategory_Click(object sender, EventArgs e)
        {
            if(dataGridViewMenu.SelectedRows.Count>0)
            {


                //można prościej!!!
                
                int  index = dataGridViewMenu.SelectedRows[0].Index;
                index++;
                         switch(index)
                {
                    case 1:
                        {
                            MenuDataGridview.GetMenuCategoryWhere(sqlConnection, sqlDataAdapter, dataGridViewMenuCategory, "Starters","zimne");
                            break;
                        }
                    case 2:
                        {
                            MenuDataGridview.GetMenuCategoryWhere(sqlConnection, sqlDataAdapter, dataGridViewMenuCategory, "Starters", "ciepłe");
                            break;
                        }
                    case 3:
                        {
                            MenuDataGridview.GetMenuCategory(sqlConnection, sqlDataAdapter, dataGridViewMenuCategory, "Soups");
                            break;
                        }
                    case 4:
                        {
                            MenuDataGridview.GetMenuCategoryWhere(sqlConnection, sqlDataAdapter, dataGridViewMenuCategory, "MainDishes", "jarskie");
                            break;
                        }
                    case 5:
                        {
                            MenuDataGridview.GetMenuCategoryWhere(sqlConnection, sqlDataAdapter, dataGridViewMenuCategory, "MainDishes", "mięsne");
                            break;
                        }
                    case 6:
                        {
                            MenuDataGridview.GetMenuCategory(sqlConnection, sqlDataAdapter, dataGridViewMenuCategory, "Salads");
                            break;
                        }
                    case 7:
                        {
                            MenuDataGridview.GetMenuCategoryWhere(sqlConnection, sqlDataAdapter, dataGridViewMenuCategory, "Drinks", "zimne");
                            break;
                        }
                    case 8:
                        {
                            MenuDataGridview.GetMenuCategoryWhere(sqlConnection, sqlDataAdapter, dataGridViewMenuCategory, "Drinks", "ciepłe");
                            break;
                        }
                    case 9:
                        {
                            MenuDataGridview.GetMenuCategoryWhere(sqlConnection, sqlDataAdapter, dataGridViewMenuCategory, "Drinks", "alkoholowe");
                            break;
                        }
                    case 10:
                        {
                            MenuDataGridview.GetMenuCategory(sqlConnection, sqlDataAdapter, dataGridViewMenuCategory, "Desserts");
                            break;
                        }
                }

            }
            else
            {
                MessageBox.Show("Nie zaznaczono pozycji w menu!");
            }
        }

        private void buttonShowPicture_Click(object sender, EventArgs e)
        {
            if (dataGridViewMenuCategory.SelectedRows.Count > 0)
            {
                string dataDishName = dataGridViewMenuCategory.CurrentRow.Cells[0].Value.ToString();
                string dataMenu = dataGridViewMenu.CurrentRow.Cells[0].Value.ToString();
                string menuNameCategory=MenuDataGridview.GetMenuCategoryNameFromDatabase(sqlConnection, dataMenu);
                MenuDataGridview.GetDishImage(sqlConnection, menuNameCategory, dataDishName,pictureBoxDishImage);
            }
            else
            {
                MessageBox.Show("Nie zaznaczono dania!");
            }
        }

        private void buttonWorkers_Click(object sender, EventArgs e)
        {
            FormWorkers formWorkers = new FormWorkers();
            formWorkers.ShowDialog();
        }

        private void buttonClients_Click(object sender, EventArgs e)
        {
            FormClients formClients = new FormClients();
            formClients.ShowDialog();
        }
    }
}
